# README #

Clinical Pathology method validation statistics

* Series of exercises for learning about the application of statistics to clinical pathology

### Setup ###

* Download repository
    * git clone git@bitbucket.org:hermands/stats_teaching.git
    * cd stats_teaching
* Open `ref_interval.blank.Rmd` markdown file in Rstudio

### Contact ###

* daniel.herman2@pennmedicine.upenn.edu